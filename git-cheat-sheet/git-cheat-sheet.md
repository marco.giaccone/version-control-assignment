# Git Cheat Sheet

## 00. Installazione
- **download git bash**: [guida] (https://www.oracle.com/webfolder/technetwork/tutorials/ocis/ocis_fundamental/gitbash-inst.pdf)
- **sudo apt-get install git**: effettua il download e l' installazione di git. Su windows, il comando puo' essere lanciato anche da PowerShell

## 01. Configurazione
- **git config --global user.name "<*name*>"**: imposta il nome che comparira' nei commits
- **git config --global user.email "<*your@email*>"**: imposta l' indirizzo email<br>
                    `git config --global user.name "Marco Giaccone"`<br>
                    `git config --global user.email "marco.giaccone@edu.itspiemonte.it"`

## 02. Avviare un progetto
- **git init <*nome del progetto*>**: crea un nuovo repository locale
- **git clone <*url-del-progetto*>**: effettua un download di un progetto da un repository remoto, assicurati di essere nella cartella giusta<br>
                    `git clone git@gitlab.com:marco.giaccone/version-control-assignment.git`

## 03. Operazioni frequenti
- **git status**: mostra le informazioni sullo stato corrente del repository locale (file in staging, file modificati, file pronti per il commit, mostra lo stato del branch locale rispetto a quello remoto)
- **git add <*file*> *oppure* <*cartella*>**: aggiunge un file o una cartella all' area di staging<br>
                    `git add git-cheat-sheet.md`
- **git add .** : aggiunge tutti i file dalla directory corrente e sotto-directory all' area di staging
- **git diff <*file*>**: mostra le differenze tra la cartella di lavoro e l' area di staging<br>
                    `git diff git-cheat-sheet.md`
- **git checkout <*branch target*>** : cambia branch
- **git checkout -b <*nuovo branch*>**: crea un nuovo branch e ci entra<br>
                    `git checkout -b nuovo-branch`
- **git checkout --<*file*>**: ripristina *file* alla versione dell' ultimo commit
- **git commit**: crea un nuovo commit a partire dai cambiamenti aggiunti all' area di staging
- **git rm <*file*>**: rimuove *file* dalla cartella di lavoro e dall' area di staging. Aggiungere **-r <*directory*>** comporta una rimozione di tutti i file e sottocartelle contenuti nella directory selezionata, compresa la stessa directory

## 04. Archiviazione
- **git stash**: salva temporaneamente le modifiche nella cartella corrente e nella staging area non ancora committate 
- **git stash pop**: applica le modifiche salvate nella stash alla cartella corrente e rimuove quella stash dalla lista di quelle salvate
- **git stash drop**: cancella l' ultima stash

## 05. Branching
- **git branch**: visualizza la lista dei branches nella repo. Se si aggiunge **-a**, il comando fa vedere tutti i branch
- **git branch <*branch*>**: crea un nuovo branch, ma rimane su quello corrente, a cui posso passare con **git checkout <*branch name*>**<br>
                    `git branch nuovo-branch`<br>
                    `git checkout nuovo-branch`
- **git rebase <*branch*>**: release del branch corrente sul *main*
- **git checkout -b <*nuovo branch*>**: crea un nuovo branch e ci entra
- **git merge <*branch*>**: integra il branch selezionato nel branch corrente
- **git branch -d <*branch*>**: elimina il branch selezionato


## 06. History
- **git log**: elenca la cronologia del branch corrente
- **git log <*n log*>**: elenca gli ultimi **n** commits<br>
                    `git log -n 5 `

## 07. Tags
- **git tag -a <*versione*> -m "<*messaggio*>"**: crea un tag<br>
                    `git tag -a v1.0 -m "git-cheat-sheet"`<br>
                    `git push origin v1.0`
- **git tag**: elenca tutti i tag
- **git tag**: crea un tag per il commit corrente. Possiamo aggiungere **commit sha** per taggare un commit specifico
- **git tag -d <*name*>**: rimuove un tag dal repository locale

## 08. Revocare le modifiche
- **git reset <*commit*>**: sposta il branch corrente ad uno specifico commit. Se si aggiunge **--hard**, scarta tutte le modifiche. Rischio di perdita delle modifiche non committate.
- **git revert <*commit*>**: annulla i cambiamenti introdotti da un singolo commit o da una serie di commit, crea un nuovo commit che rappresenta l'effetto contrario di questi cambiamenti

## 09. Sicronizzazione dei repositories
- **git fetch <*repo remoto*>**: recupera le informazioni dal repository remoto senza integrarle direttamente nel branch locale corrente, a differenza di git pull.
- **git pull <*repo remoto*>**: recuperare le modifiche dal repository remoto specificato  e integrarle nel branch corrente del tuo repository locale
- **git push <*--tags*> <*repo remoto*>**: pusha i cambiamenti fatti in locale sul repo remoto selezionato
- **git push -u <*repo remoto*> <*branch*>**: pusha un branch locale nel repo remoto selzionato








